tags = {
  Application = "Demo App 1"
  Title       = "Terraform + Gitlab CI"
  Description = "This is a demo application 1 for HashiTalks Africa"
  Environment = "Stage - Demo"
  Managed-By  = "Terraform"
  region      = "us-west-2"
}