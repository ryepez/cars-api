module "s3" {
  source = "../../modules/s3"

  bucket = "${local.application}-random425"
  tags   = var.tags
}