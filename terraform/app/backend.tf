terraform {
  backend "s3" {
    bucket = "ronnyhashitalks.state"
    region = "us-west-2"
    key    = "myapp/terraform.tfstate"
  }
}